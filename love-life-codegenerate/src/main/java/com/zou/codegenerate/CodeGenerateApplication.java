package com.zou.codegenerate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * @BelongsProject: love-life
 * @BelongPackage: com.zou.codegenerate
 * @Author: zouzhimin
 * @Date: 2020/11/25 15:12
 * @Description: TODO
 **/
@ServletComponentScan
@SpringBootApplication
public class CodeGenerateApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(CodeGenerateApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(CodeGenerateApplication.class, args);
    }
}
