package com.zou.security.base.enums;

/**
 * @BelongsProject: love-life
 * @BelongPackage: com.zou.security.base.enums
 * @Author: zouzhimin
 * @Date: 2020/11/25 11:53
 * @Description: TODO
 **/
public enum  UserEnum {

    /**
     * 匿名用户
     */
    ANONYMOUSUSER("anonymousUser");

    private String user;

    UserEnum(String user){
        this.user = user;
    }

    public String getUser(){
        return user;
    }
}
