package com.zou.security.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zou.security.base.dto.sysrole.AddDto;
import com.zou.security.base.dto.sysrole.FindByPageDto;
import com.zou.security.base.dto.sysrole.OauthDto;
import com.zou.security.base.dto.sysrole.UpdateDto;
import com.zou.security.base.http.Request;
import com.zou.security.base.http.Response;
import com.zou.security.base.pojo.SysRole;
import com.zou.security.base.pojo.SysRoleMenu;
import com.zou.security.service.ISysRoleMenuService;
import com.zou.security.service.ISysRoleService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.zzm.zlog.inter.ZLog;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 邹智敏
 * @since 2020-08-11
 */
@RestController
@RequestMapping("/sysRole")
public class SysRoleController {

    @Autowired
    private ISysRoleService iSysRoleService;

    @Autowired
    private ISysRoleMenuService iSysRoleMenuService;

    @PostMapping("/findByPage")
    @ZLog("角色分页")
    public Response findByPage(@Valid @RequestBody FindByPageDto findByPage) {

        //这里进行了模糊查询+分页查询条件
        IPage<SysRole> sysRoleIPage = iSysRoleService.selectPageVo(new Page<>(findByPage.getPage(),findByPage.getLimit()));

        return Request.success(sysRoleIPage.getRecords(),sysRoleIPage.getTotal());
    }

    @PostMapping("/add")
    @ZLog("角色新增")
    public Response add(@Valid @RequestBody AddDto add) {

        SysRole sysRole = new SysRole();
        BeanUtils.copyProperties(add, sysRole);

        if (iSysRoleService.save(sysRole)) {
            return Request.success();
        }

        return Request.error();
    }

    @PostMapping("/update")
    @ZLog("角色修改")
    public Response update(@Valid @RequestBody UpdateDto update) {

        SysRole sysRole = new SysRole();
        BeanUtils.copyProperties(update, sysRole);
        if (iSysRoleService.updateById(sysRole)) {
            return Request.success();
        }

        return Request.error();
    }

    @PostMapping("/oauth")
    @ZLog("角色授权")
    public Response oauth(@Valid @RequestBody OauthDto oauth) {


        if (!oauth.getMenuIds().isEmpty()) {
            //删除原先的权限
            iSysRoleMenuService.remove(new QueryWrapper<SysRoleMenu>().lambda().eq(SysRoleMenu::getRoleId, oauth.getId()));

            List<SysRoleMenu> sysRoleMenus = new ArrayList<>();
            oauth.getMenuIds().forEach(menuId -> {
                SysRoleMenu sysRoleMenu = new SysRoleMenu();
                sysRoleMenu.setRoleId(oauth.getId());
                sysRoleMenu.setMenuId(menuId);
                sysRoleMenus.add(sysRoleMenu);
            });
            iSysRoleMenuService.saveBatch(sysRoleMenus,20000);
            return Request.success();
        }


        return Request.error();
    }

    @GetMapping("/delete/{id}")
    @ZLog(value = "角色删除")
    public Response delete(@PathVariable String id) {
        if (iSysRoleService.remove(new QueryWrapper<SysRole>().lambda().eq(SysRole::getId, id))) {
            //删除角色关联表
            iSysRoleMenuService.remove(new QueryWrapper<SysRoleMenu>().lambda().eq(SysRoleMenu::getRoleId,id));
            return Request.success();
        }
        return Request.error();

    }

    @PostMapping("/selectList")
    @ZLog(value = "角色下拉")
    public Response selectList() {
        return Request.success(iSysRoleService.list(new QueryWrapper<SysRole>().lambda().select(SysRole::getId, SysRole::getNameZh)));
    }

}

