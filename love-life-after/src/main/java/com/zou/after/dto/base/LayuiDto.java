package com.zou.after.dto.base;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：layui分页
 * @date：crealed in 上午 11:53 2019/4/11 0011
 * </P>
 **/
@Data
@EqualsAndHashCode(callSuper = false)
public class LayuiDto {

    @NotNull(message = "page参数不能为空")
    @Min(value= 0, message = "page最小值要大于0")
    private Integer page;

    @Min(value = 1,message = "最小为1")
    @Max(value = 100000,message = "最大不能超过100000")
    private Integer limit;

    public Integer getPage() {
        return (page - 1) *  this.getLimit();
    }
}
