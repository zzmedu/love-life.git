package com.zou.security.base.enums;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 14:08 2020/7/9
 * </P>
 **/
public enum ContextTypeEnum {

    JSONUTF8("application/json;charset=utf-8");

    private String contextType;

    ContextTypeEnum(String contextType) {
        this.contextType = contextType;
    }

    public String getContextType() {
        return contextType;
    }
}
