#### love-life-codegenerate 微服务
##### love-life-codegenerate 构建初衷
    有人不是说；那个mybatis和mybatis-plus都提供了代码生成器，
    为什么不使用呢？是的，确实都提供了；但是本人觉得它们提供的还不是那么完美；那又有人说了；
    mybatis-plus不是提供了3个模板引擎吗？也提供了自定义模板文件入口；
    都已经很完善了；你怎么还是觉得不完美呢；这里我要说一下；
    提供了这些虽然用起来比较还可以，但是要开发一个新的项目后，都要引入mybatis-plus 和引擎jar;工作都是反反复复，
    而且数据源和表都要在代码生成器代码里面要反反复复修改；我自己都觉得烦；如果做一个在线可配置数据源和可勾选表，以及在线编辑模板文件；那么以后每次新建新项目都可以和引擎jar说拜拜了；代码生成器都不用去写喽；全部在线配置；配置，配置，配置，爽翻天；下面我就开始介绍这款参评的启动和使用

##### love-life-codegenerate启动
+ 启动前，要先导入两个表；code_datasource_config和code_template_config；
+ 目前表在：/db/love-life.sql中；
+ 导入后，要在你love-life-codegenerate项目的application.yml配置中修改数据源
+ 启动成功如下图：
![image.png](https://upload-images.jianshu.io/upload_images/13524020-f2cd664ced617b89.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

##### love-life-codegenerate 访问成功

    访问路径：localhost:8082

![image.png](https://upload-images.jianshu.io/upload_images/13524020-2467df05e3b590f9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

##### love-life-codegenerate 生成代码
![image.png](https://upload-images.jianshu.io/upload_images/13524020-9334ed4f8925681c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

##### love-life-codegenerate 模板管理；

    这里只支持velocity引擎语法

![image.png](https://upload-images.jianshu.io/upload_images/13524020-b81df7dfd826225d.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
