package com.zou.security.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zou.security.base.pojo.SysRoleMenu;
import com.zou.security.mapper.SysRoleMenuMapper;
import com.zou.security.service.ISysRoleMenuService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 邹智敏
 * @since 2020-08-11
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements ISysRoleMenuService {

}
