package com.zou.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zou.security.base.vo.sysmenu.FindByPageVo;
import com.zou.security.base.pojo.SysMenu;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 邹智敏
 * @since 2020-08-11
 */
public interface ISysMenuService extends IService<SysMenu> {
    List<FindByPageVo> findByPage(String id);
    List<FindByPageVo> leftSideMenu(String roleName);
}
