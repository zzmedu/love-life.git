package com.zou.after.controller;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.zou.after.dto.MoneyPayAddDto;
import com.zou.after.dto.MoneyPayFindByPageDto;
import com.zou.after.dto.MoneyPayUpdateDto;
import com.zou.after.entity.MoneyPay;
import com.zou.after.service.IMoneyPayService;
import com.zou.security.base.http.Request;
import com.zou.security.base.http.Response;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.zzm.zlog.inter.ZLog;

import javax.validation.Valid;
import java.util.HashMap;

@RestController
@RequestMapping("/moneyPay")
public class MoneyPayController {

    @Autowired
    private IMoneyPayService iMoneyPayService;
    
    @PostMapping("/findByPage")
    @ZLog(value = "分页查询")
    public Response findByPage(@Valid @RequestBody MoneyPayFindByPageDto moneyPayFindByPageDto){
        return Request.success(iMoneyPayService.findByPage(moneyPayFindByPageDto),iMoneyPayService.findByPageCount(moneyPayFindByPageDto));
    }

    @PostMapping("/add")
    @ZLog(value = "新增")
    public Response add(@Valid @RequestBody MoneyPayAddDto moneyPayAddDto){
        MoneyPay moneyPay = new MoneyPay();
        BeanUtils.copyProperties(moneyPayAddDto, moneyPay);
        moneyPay.setUtime(DateUtil.now());
        if(iMoneyPayService.save(moneyPay)){
            return  Request.success();
        }
        return Request.error();
    }

    @GetMapping("/isDelete/{id}/{status}")
    @ZLog(value = "逻辑删除数据")
    public Response isDelete(@PathVariable Integer id,@PathVariable Integer status){
        if(iMoneyPayService.update(new MoneyPay(), new UpdateWrapper<MoneyPay>().lambda()
                .set(MoneyPay::getStatus,status)
                .eq(MoneyPay::getId,id))){
            return  Request.success();
        }
        return Request.error();
    }
    
    @GetMapping("/delete/{id}")
    @ZLog(value = "非逻辑删除数据")
    public Response delete(@PathVariable Integer id){
        if(iMoneyPayService.remove(new QueryWrapper<MoneyPay>().lambda().eq(MoneyPay::getId,id))){
            return Request.success();
        }
        return Request.error();
    }

    @PostMapping("/update")
    @ZLog(value = "修改")
    public Response update(@Valid @RequestBody MoneyPayUpdateDto moneyPayUpdateDto){
        MoneyPay moneyPay = new MoneyPay();
        BeanUtils.copyProperties(moneyPayUpdateDto,moneyPay);
        moneyPay.setUtime(DateUtil.now());
        if(iMoneyPayService.update(moneyPay,new QueryWrapper<MoneyPay>().lambda().eq(MoneyPay::getId,moneyPayUpdateDto.getId()))){
            return Request.success();
        }
        return Request.error();
    }

    @GetMapping("/selectOne/{id}")
    @ZLog(value = "单个查询")
    public Response selectOne(@PathVariable Integer id){
        return Request.success(iMoneyPayService.getOne(new QueryWrapper<MoneyPay>().lambda().eq(MoneyPay::getId,id)));
    }
}