package com.zou.after.service.base;

import java.util.List;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 11:31 2019/9/3
 * </P>
 **/
public interface IFindByPageService<T,V> {

    /**
     * 分页查询
     * @param t 查询条件
     * @return List<V>
     */
    List<V> findByPage(T t);

    /**
     * 分页查询总数
     * @param t 查询条件
     * @return int
     */
    Integer findByPageCount(T t);
}
