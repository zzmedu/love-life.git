package com.zou.security.base.http;



import com.zou.security.base.enums.HttpEnum;

import java.io.Serializable;
import java.sql.PreparedStatement;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 11:16 2019/4/24
 * </P>
 **/
public final class Request implements Serializable {

    private static final long serialVersionUID = 1725159680599612404L;

    public static Response success() {
        Response request = new Response();
        request.setCode(HttpEnum.SUCCESS.getCode());
        request.setMsg(HttpEnum.SUCCESS.getMsg());
        return request;
    }

    public static Response success(Object object) {
        Response request = new Response();
        request.setCode(HttpEnum.SUCCESS.getCode());
        request.setMsg(HttpEnum.SUCCESS.getMsg());
        request.setData(object);
        return request;
    }

    public static Response success(Object object,long count) {
        Response request = new Response();
        request.setCode(HttpEnum.SUCCESS.getCode());
        request.setMsg(HttpEnum.SUCCESS.getMsg());
        request.setData(object);
        request.setCount(count);
        return request;
    }

    public static Response success(int code, String msg, Object object) {
        Response request = new Response();
        request.setCode(code);
        request.setMsg(msg);
        request.setData(object);
        return request;
    }

    public static Response error() {
        Response request = new Response();
        request.setCode(HttpEnum.fail.getCode());
        request.setMsg(HttpEnum.fail.getMsg());
        return request;
    }

    public static Response error(int code, String msg) {
        Response request = new Response();
        request.setCode(code);
        request.setMsg(msg);
        return request;
    }
}
