package com.zou.security.base.dto.sysrole;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 18:28 2020/8/11
 * </P>
 **/
@Setter
@Getter
public class AddDto {

    @NotBlank(message = "角色名称【定制】不能为空")
    private String name;

    @NotBlank(message = "角色名称【中文】不能为空")
    private String nameZh;

}
