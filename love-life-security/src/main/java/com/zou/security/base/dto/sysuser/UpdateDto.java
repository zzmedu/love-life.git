package com.zou.security.base.dto.sysuser;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 18:13 2020/8/11
 * </P>
 **/
@Setter
@Getter
public class UpdateDto {

    private Long id;

    @NotBlank(message = "昵称不能为空")
    private String nikName;

    private String email;

    @NotBlank(message = "语言不能为空")
    private String lang;

    @NotNull(message = "请选择角色")
    private Long roleId;
}
