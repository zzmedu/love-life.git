package com.zou.after.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class DynamicTaskAddDto {

    @NotBlank(message = "定时任务名称不能为空")
    private String name;

    @NotBlank(message = "负责人名称不能为空")
    private String bear;

    @NotBlank(message = "定时任务表达式不能为空")
    private String cron;

    private String desc;

    @NotBlank(message = "类路径不能为空")
    private String classPath;
}
