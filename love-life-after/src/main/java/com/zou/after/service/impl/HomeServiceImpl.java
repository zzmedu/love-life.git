package com.zou.after.service.impl;

import com.zou.after.mapper.HomeMapper;
import com.zou.after.service.IHomeService;
import com.zou.after.vo.HomeAweekDiscountVo;
import com.zou.after.vo.HomeIncomeToDayVo;
import com.zou.security.base.http.Request;
import com.zou.security.base.http.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class HomeServiceImpl implements IHomeService {

    @Autowired
    private HomeMapper homeMapper;

    @Override
    public HomeIncomeToDayVo incomeTypeToDay(String time) {
        return homeMapper.incomeTypeToDay(time);
    }

    @Override
    public HomeIncomeToDayVo incomeModeToDay(String time) {
        return homeMapper.incomeModeToDay(time);
    }

    @Override
    public HomeIncomeToDayVo payModeToDay(String time) {
        return homeMapper.payModeToDay(time);
    }

    @Override
    public HomeIncomeToDayVo payTypeToDay(String time) {
        return homeMapper.payTypeToDay(time);
    }

    @Override
    public Response aweekDiscount(String stime, String etime) {
        HashMap hashMap = new HashMap();
        List<HomeAweekDiscountVo> homeAweekDiscountVos = homeMapper.aweekDiscount(stime, etime);
        List<String> date = new ArrayList<>();
        List<String> incomeMoney = new ArrayList<>();
        List<String> payMoney = new ArrayList<>();
        if (!homeAweekDiscountVos.isEmpty()) {
            homeAweekDiscountVos.forEach(homeAweekDiscountVo -> {
                date.add(homeAweekDiscountVo.getDate());
                incomeMoney.add(homeAweekDiscountVo.getIncomeMoney());
                payMoney.add(homeAweekDiscountVo.getPayMoney());
            });
        }
        hashMap.put("date",date);
        hashMap.put("incomeMoney",incomeMoney);
        hashMap.put("payMoney",payMoney);
        return Request.success(hashMap);
    }
}
