```
__                                              __        __   ______
/  |                                            /  |      /  | /      \
$$ |        ______   __     __  ______          $$ |      $$/ /$$$$$$  |______
$$ |       /      \ /  \   /  |/      \  ______ $$ |      /  |$$ |_ $$//      \
$$ |      /$$$$$$  |$$  \ /$$//$$$$$$  |/      |$$ |      $$ |$$   |  /$$$$$$  |
$$ |      $$ |  $$ | $$  /$$/ $$    $$ |$$$$$$/ $$ |      $$ |$$$$/   $$    $$ |
$$ |_____ $$ \__$$ |  $$ $$/  $$$$$$$$/         $$ |_____ $$ |$$ |    $$$$$$$$/
$$       |$$    $$/    $$$/   $$       |        $$       |$$ |$$ |    $$       |
$$$$$$$$/  $$$$$$/      $/     $$$$$$$/         $$$$$$$$/ $$/ $$/      $$$$$$$/
```

<p align="center">
    <a target="_blank" href="https://www.oracle.com/technetwork/java/javase/downloads/index.html"><img src="https://img.shields.io/badge/JDK-1.8+-green.svg" /></a>
    <a target="_blank" href="http://zzmblog.top"><img src="https://img.shields.io/badge/Docs-latest-blue.svg"/></a>
    <a target="_blank" href='https://gitee.com/zzmedu/love-life'><img src="https://gitee.com/ssssssss-team/magic-api/badge/star.svg?theme=white" /></a>
    <a target="_blank" href='https://gitee.com/zzmedu/love-life'><img src="https://img.shields.io/github/stars/ssssssss-team/magic-api.svg?style=social"/></a>
    <a target="_blank" href="LICENSE"><img src="https://img.shields.io/:license-MIT-blue.svg"></a>
</p>

# 简介
love-life 是基于springboot 2.3.6，maven多模块；包含了在线可视化代码生成器；安全认证中心；love-life 后台接口;以及 love-life 后台管理界面；一个敏捷开发项目；

# 部署
* 下载源码到本地：git clone https://gitee.com/zzmedu/love-life.git
* 把数据表导入数据库中，数据表：<a href="https://gitee.com/zzmedu/love-life/raw/master/db/love-life.sql">/love-life/db/love-life.sql</a>
* 用nginx部署前端项目；前端项目为：/love-life/liove-life-front里面有说明.txt![](https://gitee.com/zzmedu/project-picture/raw/master/love-life-doc-img/sm.jpg)

* 后台启动类：/love-life/love-life-after/src/main/java/com/zou/after/AfterApplication.java
* nginx动态代理后台接口；解决前后台分离跨域问题；
*
        server {
            listen       8868;
            server_name  localhost;

    		#允许快要请求的域，*代表所有
    		add_header 'Access-Control-Allow-Origin' *;
    		#允许带上cookie请求
    		add_header 'Access-Control-Allow-Credentials' 'true';
    		#允许请求的方法，比如 GET/POST/PUT/DELETE
    		add_header 'Access-Control-Allow-Methods' *;
    		#允许请求的header
    		add_header 'Access-Control-Allow-Headers' *;

            # 访问前端接口
    		location / {
    		    root   D:/personalProjects/love-life/love-life-front;
    		    index  index.html index.htm;
    		}

            # 代理adxdc后端接口
            location /loveLifeAfter {
    	        proxy_set_header Cookie $http_cookie;
    	        proxy_set_header X-Forwarded-Host $host;
    	        proxy_set_header X-Forwarded-Server $host;
    	        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    	        proxy_set_header X-real-ip $remote_addr;
                proxy_pass   http://127.0.0.1:8081/loveLifeAfter;
            }
        }
>  后端访问入口：http://localhost:8868/start

> 超级管理员账号：admin
超级管理员密码：123456

> 普通管理员账号：cw
普通管理员密码：cw123

# 特性
+ 基于spring scurity 5.x完成了RBAC权限认证
+ 前后端分离；接口全程使用token认证
+ 支持代码在线生成

# 目录介绍
|文件名称|文件说明|
|--|--|
|db|数据库表
|love-life-after|系统后台接口
|love-life-codegenerate|在线代码生成器
|love-life-common|通用公共组件模块
|love-life-front|系统后台前端页面
|love-life-security|系统安全认证中心
|pom.xml|父级maven配置文件

# 项目截图
## 整体截图
![image.png](https://gitee.com/zzmedu/project-picture/raw/master/love-life-doc-img/zt.png)

## 权限认证
|账号管理|角色管理|菜单管理
|--|--|--|
|![账号管理](https://gitee.com/zzmedu/project-picture/raw/master/love-life-doc-img/zh.png)|![角色管理](https://gitee.com/zzmedu/project-picture/raw/master/love-life-doc-img/js.png)|![菜单管理](https://gitee.com/zzmedu/project-picture/raw/master/love-life-doc-img/cd.png)|

## 系统工具
![内嵌gitee](https://gitee.com/zzmedu/project-picture/raw/master/love-life-doc-img/gitee.png)
![内嵌百度](https://gitee.com/zzmedu/project-picture/raw/master/love-life-doc-img/baidu.png)
![内嵌layui文档](https://gitee.com/zzmedu/project-picture/raw/master/love-life-doc-img/layui.png)

# 代码在线生成
## 生成代码
![生成代码](https://gitee.com/zzmedu/project-picture/raw/master/love-life-doc-img/scdm.png)
## 模板管理
![image.png](https://gitee.com/zzmedu/project-picture/raw/master/love-life-doc-img/mbgl.png)

# 其它开源项目
- [spring-security-demo RBAC安全认证demo](https://gitee.com/zzmedu/spring-security-demo)
- [zlog-spirng-boot-start 基于spring boot自定义日志打印jar,可加入任何项目中去；引用：@ZLog(value = "分页查询")](https://gitee.com/zzmedu/zlog-spirng-boot-start)