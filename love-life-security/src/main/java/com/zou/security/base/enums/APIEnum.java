package com.zou.security.base.enums;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 14:43 2020/7/9
 * </P>
 **/
public enum APIEnum {

    /**
     * 下面为需要放开的接口
     */
    VERCODE("/vercode"),

    LOGIN("/login"),

    LOGOUT("/logout"),

    FAVICONICO("/favicon.ico"),

    DOC("/doc.html"),

    WEBJARS("/webjars/**"),

    SWAGGERRESOURCES("/swagger-resources/**"),

    v2ApiDocs("/v2/api-docs"),

    v2ApiDocsExt("/v2/api-docs-ext"),

    REMEMBERME("remember-me"),

    LEFTMENU("/sysMenu/leftSideMenu");

    private String api;

    APIEnum(String api){
        this.api = api;
    }

    public String getApi(){
        return api;
    }
}
