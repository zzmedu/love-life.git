package com.zou.common;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：防止表单重复提交问题
 * @date：crealed in 11:43 2020/5/26
 * </P>
 **/
@Target(value = {ElementType.METHOD})
@Retention(value = RetentionPolicy.RUNTIME)
public @interface NoRequestSubmit {
    //失效时间，默认3秒
    int value() default 5;
}
