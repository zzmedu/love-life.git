package com.zou.security.base.dto.syslog;

import lombok.Getter;
import lombok.Setter;

/**
 * @BelongsProject: spring-security-demo
 * @BelongPackage: com.huntmobi.springsecuritydemo.base.log
 * @Author: zouzhimin
 * @Date: 2020/11/19 11:29
 * @Description: 表单登录，自定义登录参数格式
 **/
@Setter
@Getter
public class FormLoginDto {

    private String username;
    private String password;
    private String dyCode;
}
