package com.zou.security.base.enums;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 14:16 2020/7/9
 * </P>
 **/
public enum TokenEnum {

    TOKENHEADER(200,"authorization"),

    TOKENEMPTY(404,"token不能为空"),

    TOKENPREFIX(200,"Bearer "),

    TOKENILLEGAL(401,"非法请求，请先进行登录"),

    TOKENINVALID(200,"Token已失效，请重新登录"),

    TOKENFORMAT(500,"token格式不正确，导致token解析失败，请重新登录");

    private Integer code;

    private String msg;

    TokenEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
