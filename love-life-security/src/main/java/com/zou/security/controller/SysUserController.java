package com.zou.security.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zou.security.base.dto.sysuser.AddDto;
import com.zou.security.base.dto.sysuser.FindByPageDto;
import com.zou.security.base.dto.sysuser.UpdateDto;
import com.zou.security.base.enums.HttpEnum;
import com.zou.security.base.http.Response;
import com.zou.security.base.http.Request;
import com.zou.security.base.pojo.SysUser;
import com.zou.security.base.pojo.SysUserRole;
import com.zou.security.base.util.SecurityUtil;
import com.zou.security.base.vo.sysuser.FindByPageVo;
import com.zou.security.service.ISysUserRoleService;
import com.zou.security.service.ISysUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.zzm.zlog.inter.ZLog;

import javax.validation.Valid;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 邹智敏
 * @since 2020-08-11
 */
@RestController
@RequestMapping("/sysUser")
public class SysUserController {

    @Autowired
    private ISysUserService iSysUserService;
    @Autowired
    private ISysUserRoleService iSysUserRoleService;

    @PostMapping("/findByPage")
    @ZLog(value = "账户分页")
    public Response findByPage(@Valid @RequestBody FindByPageDto findByPageDto){

        //这里进行了模糊查询+分页查询条件
        IPage<FindByPageVo> sysUserIPage = iSysUserService.findByPage(new Page<>(findByPageDto.getPage(),findByPageDto.getLimit()));

        return Request.success(sysUserIPage.getRecords(),sysUserIPage.getTotal());
    }

    @PostMapping("/add")
    @ZLog(value = "账户新增")
    public Response add(@Valid @RequestBody AddDto add){
        //判断是否存在账号
        if(iSysUserService.getOne(new QueryWrapper<SysUser>().lambda().eq(SysUser::getUsername,add.getUsername())) != null){
            return Request.error(HttpEnum.ISEXIS.getCode(), HttpEnum.ISEXIS.getMsg());
        }
        //密码加密
        add.setPassword(SecurityUtil.getPassword(add.getUsername()+"123456"));

        SysUser sysUser = new SysUser();
        BeanUtils.copyProperties(add,sysUser);

        if(iSysUserService.save(sysUser)){
            //开始新增角色联名表数据
            SysUserRole sysUserRole = new SysUserRole();
            sysUserRole.setUserId(iSysUserService.getOne(new QueryWrapper<SysUser>().lambda().select(SysUser::getId).eq(SysUser::getUsername,add.getUsername())).getId());
            sysUserRole.setRoleId(add.getRoleId());
            if(iSysUserRoleService.save(sysUserRole)){
                return Request.success();
            }

        }

        return Request.error();
    }

    @PostMapping("/update")
    @ZLog(value = "账户修改")
    public Response update(@Valid @RequestBody UpdateDto update){

        SysUser sysUser = new SysUser();
        BeanUtils.copyProperties(update,sysUser);

        if(iSysUserService.updateById(sysUser)){
            if(iSysUserRoleService.update(new SysUserRole(),
                    new UpdateWrapper<SysUserRole>().lambda()
                            .set(SysUserRole::getRoleId,update.getRoleId())
                            .eq(SysUserRole::getUserId,update.getId()))){
                return Request.success();
            }

        }

        return Request.error();
    }

    @PostMapping("/selectList")
    @ZLog(value = "账户下拉")
    public Response selectList(){
        return Request.success(iSysUserService.list(new QueryWrapper<SysUser>().lambda()
                .select(SysUser::getId,SysUser::getUsername)));
    }
}

