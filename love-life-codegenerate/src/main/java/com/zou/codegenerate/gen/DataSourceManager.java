package com.zou.codegenerate.gen;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.HikariJNDIFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.jdbc.DataSourceBuilder;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author tanghc
 */
public class DataSourceManager {

    private static final Logger logger = LoggerFactory.getLogger(DataSourceManager.class);


    private static final Map<String, DataSource> DATA_SOURCE_MAP = new ConcurrentHashMap<>(16);
    private static final ThreadLocal<Connection> CONNECTION_LOCAL = new ThreadLocal<>();

    public static Connection getConnection(GeneratorConfig generatorConfig) {
        Connection connection = CONNECTION_LOCAL.get();
        if (connection == null) {
            try {
                connection = getDataSource(generatorConfig).getConnection();
                CONNECTION_LOCAL.set(connection);
            } catch (SQLException e) {
                logger.error("获取Connection失败, jdbcUrl:{}", generatorConfig.getJdbcUrl(), e);
                throw new RuntimeException("获取Connection失败", e);
            }
        }
        return connection;
    }

    public static void closeConnection() {
        Connection connection = CONNECTION_LOCAL.get();
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        CONNECTION_LOCAL.remove();
    }

    public static DataSource getDataSource(GeneratorConfig generatorConfig) {
        String jdbcUrl = generatorConfig.getJdbcUrl();
        DataSource dataSource = DATA_SOURCE_MAP.computeIfAbsent(jdbcUrl, key -> {

            Properties properties = new Properties();
            properties.put("driverClassName", generatorConfig.getDriverClass());
            properties.put("jdbcUrl", generatorConfig.getJdbcUrl());
            properties.put("username", generatorConfig.getUsername());
            properties.put("password", generatorConfig.getPassword());

            try {
                //使用HikariCP数据源
                return new HikariDataSource(new HikariConfig(properties));
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        });
        if (dataSource == null) {
            throw new RuntimeException("连接数据库失败");
        }
        return dataSource;
    }

}
