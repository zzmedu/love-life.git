package com.zou.after.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class DynamicTaskUpdateDto extends DynamicTaskAddDto{

    @NotBlank(message = "编号不能为空")
    private Long id;
}
