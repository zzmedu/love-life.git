package com.zou.after.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class MoneyMode {
	/** 编号 */
	@TableId(value = "id",type= IdType.AUTO)
	private Long id;
	/** 支付方式名称 */
	private String name;
	/** 逻辑删除；1：正常；2：删除*/
	private Integer status;
}