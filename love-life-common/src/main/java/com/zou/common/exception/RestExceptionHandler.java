package com.zou.common.exception;


/**
 * Created by 廖师兄
 * 2017-01-21 14:05
 */
public class RestExceptionHandler extends RuntimeException{

    private Integer code;

    public RestExceptionHandler(int code,String msg) {
        super(msg);
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
