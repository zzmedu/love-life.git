package com.zou.after.controller;

import cn.hutool.core.date.DateUtil;
import com.zou.after.service.IHomeService;
import com.zou.security.base.http.Request;
import com.zou.security.base.http.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.zzm.zlog.inter.ZLog;

@RestController
@RequestMapping("/home")
public class HomeController {

    @Autowired
    private IHomeService iHomeService;

    /**
     * 收入方式今日
     */
    @GetMapping("/incomeModeToDay")
    @ZLog("收入方式【今日】")
    public Response incomeModeToDay(){
        return Request.success(iHomeService.incomeModeToDay(DateUtil.today()));
    }

    /**
     * 收入类型今日
     */
    @GetMapping("/incomeTypeToDay")
    @ZLog("收入类型【今日】")
    public Response incomeTypeToDay(){
        return Request.success(iHomeService.incomeTypeToDay(DateUtil.today()));
    }

    /**
     * 支出方式今日
     */
    @GetMapping("/payModeToDay")
    @ZLog("支出方式【今日】")
    public Response payModeToDay(){
        return Request.success(iHomeService.payModeToDay(DateUtil.today()));
    }

    /**
     * 支出类型今日
     */
    @GetMapping("/payTypeToDay")
    @ZLog("支出类型【今日】")
    public Response payTypeToDay(){
        return Request.success(iHomeService.payTypeToDay(DateUtil.today()));
    }

    /**
     * 近七日折现统计图
     */
    @GetMapping("/aweekDiscount")
    @ZLog("一周收入和支出折现统计图")
    public Response aweekDiscount(){
        return iHomeService.aweekDiscount(DateUtil.formatDate(DateUtil.offsetDay(DateUtil.lastWeek(),+1)),DateUtil.today());
    }
}
