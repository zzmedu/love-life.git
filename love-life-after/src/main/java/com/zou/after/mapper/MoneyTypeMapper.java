package com.zou.after.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zou.after.mapper.base.FindByPageMapper;
import com.zou.after.dto.MoneyTypeFindByPageDto;
import com.zou.after.vo.MoneyTypeFindByPageVo;
import com.zou.after.entity.MoneyType;

public interface MoneyTypeMapper extends BaseMapper<MoneyType>,FindByPageMapper<MoneyTypeFindByPageDto,MoneyTypeFindByPageVo>{

}