package com.zou.security.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zou.security.base.vo.sysmenu.FindByPageVo;
import com.zou.security.base.pojo.SysMenu;
import com.zou.security.mapper.SysMenuMapper;
import com.zou.security.service.ISysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 邹智敏
 * @since 2020-08-11
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements ISysMenuService {

    @Autowired
    private SysMenuMapper sysMenuMapper;

    @Override
    public List<FindByPageVo> findByPage(String id) {
        if(StringUtils.isEmpty(id)){
            return sysMenuMapper.findByPage(null);
        }
        return sysMenuMapper.findByPage(Long.parseLong(id));
    }

    @Override
    public List<FindByPageVo> leftSideMenu(String roleName) {
        return sysMenuMapper.leftSideMenu(roleName);
    }
}
