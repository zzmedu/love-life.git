package com.zou.security.base.dto.sysmenu;


import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 9:48 2020/8/13
 * </P>
 **/
@Setter
@Getter
public class UpdateDto extends AddDto {

    @NotNull(message = "菜单编号不能为空")
    private Long id;
}
