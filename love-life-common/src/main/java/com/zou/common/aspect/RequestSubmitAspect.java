package com.zou.common.aspect;

import com.zou.common.NoRequestSubmit;
import com.zou.common.enums.ResultEnum;
import com.zou.common.exception.RestExceptionHandler;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：防止表单重复提交
 * @date：crealed in 11:45 2020/5/26
 * </P>
 **/
@Aspect
@Component
@Slf4j
public class RequestSubmitAspect {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Around("@annotation(com.zou.common.NoRequestSubmit)")
    public Object noRequestSubmit(ProceedingJoinPoint joinPoint) throws Throwable {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        //请求路径
        String path = request.getRequestURI();
        //请求session
        String sessionId = request.getRequestedSessionId();
        //如果不存在，就存入第一次提交的基本信息
        if(ObjectUtils.isEmpty(redisTemplate.opsForValue().get(sessionId))){
            MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
            NoRequestSubmit noRequestSubmit = methodSignature.getMethod().getAnnotation(NoRequestSubmit.class);
            redisTemplate.opsForValue().set(sessionId,path,noRequestSubmit.value(), TimeUnit.SECONDS);
        }else{
            //抛出重复提交异常
            throw new RestExceptionHandler(ResultEnum.REQUESTSUBMIT.getCode(),ResultEnum.REQUESTSUBMIT.getMsg());
        }

        return joinPoint.proceed();
    }

}
