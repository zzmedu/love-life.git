package com.zou.after.mapper;

import com.zou.after.vo.HomeAweekDiscountVo;
import com.zou.after.vo.HomeIncomeToDayVo;

import java.util.List;

public interface HomeMapper{
    HomeIncomeToDayVo incomeModeToDay(String time);
    HomeIncomeToDayVo incomeTypeToDay(String time);
    HomeIncomeToDayVo payModeToDay(String time);
    HomeIncomeToDayVo payTypeToDay(String time);
    List<HomeAweekDiscountVo> aweekDiscount(String stime, String etime);
}
