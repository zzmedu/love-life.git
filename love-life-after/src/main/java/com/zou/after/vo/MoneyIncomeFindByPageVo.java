package com.zou.after.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 14:16 2019/7/19
 * </P>
 **/
@Data
@EqualsAndHashCode(callSuper = false)
public class MoneyIncomeFindByPageVo {

    private Long id;
    /** 收入方式编号 */
    private String mode;
    /** 收入方式类型 */
    private String type;
    /** 收入金额 */
    private BigDecimal money;
    /** 支付时间 */
    private String stime;
    /** 修改时间 */
    private String utime;
    /** 扣费商家 */
    private String store;
    /** 收入用户 */
    private String username;
    /** 逻辑删除；1：正常；2：删除*/
    private Integer status;
}
