package com.zou.after.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.zou.after.dto.MoneyTypeAddDto;
import com.zou.after.dto.MoneyTypeFindByPageDto;
import com.zou.after.dto.MoneyTypeUpdateDto;
import com.zou.after.entity.MoneyType;
import com.zou.after.service.IMoneyTypeService;
import com.zou.security.base.http.Request;
import com.zou.security.base.http.Response;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.zzm.zlog.inter.ZLog;

import javax.validation.Valid;
import java.util.HashMap;

@RestController
@RequestMapping("/moneyType")
public class MoneyTypeController {

    @Autowired
    private IMoneyTypeService iMoneyTypeService;
    
    @PostMapping("/findByPage")
    @ZLog(value = "分页查询")
    public Response findByPage(@Valid @RequestBody MoneyTypeFindByPageDto moneyTypeFindByPageDto){
        return Request.success(iMoneyTypeService.findByPage(moneyTypeFindByPageDto),iMoneyTypeService.findByPageCount(moneyTypeFindByPageDto));
    }

    @PostMapping("/add")
    @ZLog(value = "新增")
    public Response add(@Valid @RequestBody MoneyTypeAddDto moneyTypeAddDto){
        MoneyType moneyType = new MoneyType();
        BeanUtils.copyProperties(moneyTypeAddDto, moneyType);
        if(iMoneyTypeService.save(moneyType)){
            return  Request.success();
        }
        return Request.error();
    }

    @GetMapping("/isDelete/{id}/{status}")
    @ZLog(value = "逻辑删除数据")
    public Response isDelete(@PathVariable Integer id,@PathVariable Integer status){
        if(iMoneyTypeService.update(new MoneyType(), new UpdateWrapper<MoneyType>().lambda()
                .set(MoneyType::getStatus,status)
                .eq(MoneyType::getId,id))){
            return  Request.success();
        }
        return Request.error();
    }
    
    @GetMapping("/delete/{id}")
    @ZLog(value = "非逻辑删除数据")
    public Response delete(@PathVariable Integer id){
        if(iMoneyTypeService.remove(new QueryWrapper<MoneyType>().lambda().eq(MoneyType::getId,id))){
            return Request.success();
        }
        return Request.error();
    }

    @PostMapping("/update")
    @ZLog(value = "修改")
    public Response update(@Valid @RequestBody MoneyTypeUpdateDto moneyTypeUpdateDto){
        MoneyType moneyType = new MoneyType();
        BeanUtils.copyProperties(moneyTypeUpdateDto,moneyType);
        if(iMoneyTypeService.update(moneyType,new QueryWrapper<MoneyType>().lambda().eq(MoneyType::getId,moneyTypeUpdateDto.getId()))){
            return Request.success();
        }
        return Request.error();
    }

    @GetMapping("/selectOne/{id}")
    @ZLog(value = "单个查询")
    public Response selectOne(@PathVariable Integer id){
        return Request.success(iMoneyTypeService.getOne(new QueryWrapper<MoneyType>().lambda().eq(MoneyType::getId,id)));
    }

    @PostMapping("/selectList")
    @ZLog(value = "下拉查询")
    public Response selectList(){
        return  Request.success(iMoneyTypeService.list(new QueryWrapper<MoneyType>().lambda().select(MoneyType::getId, MoneyType::getName)));
    }
}