package com.zou.security.base.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zou.security.base.dto.syslog.FormLoginDto;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @BelongsProject: spring-security-demo
 * @BelongPackage: com.zou.security.base.filter
 * @Author: zouzhimin
 * @Date: 2020/11/19 11:40
 * @Description: 自定义登录流程
 **/
@Setter
@Getter
public class JsonLoginFilter extends UsernamePasswordAuthenticationFilter {

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        if(request.getContentType().equals(MediaType.APPLICATION_PROBLEM_JSON_UTF8_VALUE)
                || request.getContentType().equals(MediaType.APPLICATION_JSON_VALUE)){
            ObjectMapper objectMapper = new ObjectMapper();
            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = null;
            try {
                FormLoginDto formLoginDto = objectMapper.readValue(request.getInputStream(), FormLoginDto.class);
                usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(formLoginDto.getUsername(),formLoginDto.getPassword());
            } catch (IOException e) {
                e.printStackTrace();
                usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken("","");
            }finally {
                setDetails(request,usernamePasswordAuthenticationToken);
                return this.getAuthenticationManager().authenticate(usernamePasswordAuthenticationToken);
            }
        }else{
            return super.attemptAuthentication(request, response);
        }
    }
}
