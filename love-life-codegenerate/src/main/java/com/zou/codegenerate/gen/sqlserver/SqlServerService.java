package com.zou.codegenerate.gen.sqlserver;


import com.zou.codegenerate.gen.GeneratorConfig;
import com.zou.codegenerate.gen.SQLService;
import com.zou.codegenerate.gen.TableSelector;

public class SqlServerService implements SQLService {

	@Override
	public TableSelector getTableSelector(GeneratorConfig generatorConfig) {
		return new SqlServerTableSelector(new SqlServerColumnSelector(generatorConfig), generatorConfig);
	}

}
