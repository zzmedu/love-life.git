package com.zou.after.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.*;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class MoneyTypeUpdateDto extends MoneyTypeAddDto{

	/** 编号 */
	@NotBlank(message="编号不能为空")
    private Long id;
}