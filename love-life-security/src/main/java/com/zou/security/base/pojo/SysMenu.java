package com.zou.security.base.pojo;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.zou.security.base.page.PageForm;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author 邹智敏
 * @since 2020-08-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysMenu extends PageForm implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private String name;

    private String nameZh;

    private String path;

    private String icon;

    private String label;

    private Long parentId;

    private Long level;

    private Boolean isMenu;
}
