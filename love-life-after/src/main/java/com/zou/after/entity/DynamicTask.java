package com.zou.after.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class DynamicTask {

    @TableId(value = "id",type= IdType.AUTO)
    private Long id;
    private String name;
    private String cron;
    private int status;
    /** Mybatis plus 中数据库中字段有SQL关键字的处理方法 **/
    @TableField("`desc`")
    private String desc;
    private String bear;
    private String classPath;
}
