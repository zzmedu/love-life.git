package com.zou.after.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zou.after.dto.MoneyTypeFindByPageDto;
import com.zou.after.service.base.IFindByPageService;
import com.zou.after.vo.MoneyTypeFindByPageVo;
import com.zou.after.entity.MoneyType;
/**
* com.zou.after服务类
* @author zouzhimin
* @date $date.get()
*/
public interface IMoneyTypeService extends IService<MoneyType>, IFindByPageService<MoneyTypeFindByPageDto, MoneyTypeFindByPageVo>{
	
}