package com.zou.after.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zou.after.dto.MoneyPayFindByPageDto;
import com.zou.after.service.IMoneyPayService;
import com.zou.after.vo.MoneyPayFindByPageVo;
import com.zou.after.entity.MoneyPay;
import com.zou.after.mapper.MoneyPayMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MoneyPayServiceImpl extends ServiceImpl<MoneyPayMapper, MoneyPay> implements IMoneyPayService{

    @Autowired
    private MoneyPayMapper moneyPayMapper;
    
    /**
     * 分页查询
     *
     * @return 返回集合，没有返回空List
     */
    @Override
    public List<MoneyPayFindByPageVo> findByPage(MoneyPayFindByPageDto moneyPayFindByPageDto) {
        return moneyPayMapper.findByPage(moneyPayFindByPageDto);
    }

	/**
     * 分页查询总数
     *
     * @return 返回集合，没有返回空List
     */
    @Override
    public Integer findByPageCount(MoneyPayFindByPageDto moneyPayFindByPageDto) {
        return moneyPayMapper.findByPageCount(moneyPayFindByPageDto);
    }
	
}