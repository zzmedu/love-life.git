package com.zou.after.dto;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class MoneyTypeAddDto {

	/** 编号 */

	/** 类型名称；生活，采购 */
	@NotBlank(message="类型名称；生活，采购不能为空")
    private String name;
}