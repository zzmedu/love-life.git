package com.zou.security.base.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 10:40 2019/4/25
 * </P>
 **/
public class SecurityUtil {

    /**
     *  <p>@Author：zouzhimin</p>
     *  <p>@param password</p>
     *  <p>@Description：security 密码加密</p>
     *  <p>@Date：10:41 2019/4/25</p>
     **/
    public static String getPassword(String password){
        BCryptPasswordEncoder encoder =new BCryptPasswordEncoder();
        return encoder.encode(password.trim());
    }

    public static void main(String[] args) {
        System.out.println(getPassword("123456"));
    }
}
