package com.zou.security.base.page;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @BelongsProject: spring-security-demo
 * @BelongPackage: com.huntmobi.springsecuritydemo.page
 * @Author: zouzhimin
 * @Date: 2020/11/19 10:11
 * @Description: 分页类
 **/
@Setter
@Getter
public class PageForm {
    @NotNull(message = "page参数不能为空")
    @Min(value= 0, message = "page最小值要大于0")
    private Integer page;

    @Min(value = 1,message = "最小为1")
    @Max(value = 100000,message = "最大不能超过100000")
    private Integer limit;
}
