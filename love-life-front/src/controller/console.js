/**

 @Name：layuiAdmin 主页控制台
 @Author：贤心
 @Site：http://www.layui.com/admin/
 @License：LPPL

 */


layui.define(['echarts'], function (exports) {

    var $ = layui.$, echarts = layui.echarts;

    /**
     * 控制台报表分装
     */
    var home = {
        comparativeDiscount: function (options) {
            var comparativeDiscountMap = echarts.init(document.getElementById(options.id), layui.echartsTheme);
            var config = {
                title: {
                    text: options.text,
                    textStyle: {
                        fontSize: 14,
                        fontWeight: 'bolder',
                        color: '#333'
                    }
                },
                tooltip: {
                    trigger: 'axis',
                    formatter: function (params, ticket, callback) {
                        var showHtm = "";
                        var name = "";
                        var money = 0;
                        for (var i = 0; i < params.length; i++) {
                            //x轴名称
                            name = params[i][1];
                            //标题名称
                            var text = params[i].seriesName;
                            //金额
                            var value = params[i][2];
                            if (text != "收入") {
                                value = value - value * 2;
                            }

                            //计算利润
                            money += parseFloat(value);
                            //字符拼接
                            showHtm += '<br>' + text + ': ' + value + ' ($)'
                        }
                        return name + showHtm + "<br>总利润：" + money.toFixed(2) + ' ($)';
                    }
                },
                legend: {
                    data: options.ldata
                },
                calculable: true,
                xAxis: [
                    {
                        type: 'category',
                        data: options.xdata,
                        splitLine: {show: true, lineStyle: {type: 'dashed'}}
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        axisLabel: {
                            formatter: '{value}'
                        },
                        splitLine: {show: true, lineStyle: {type: 'dashed'}}
                    }
                ],
                series: [
                    {
                        name: options.ldata[0],
                        type: 'line',
                        data: options.sdata1,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    position: 'top',
                                    textStyle: {
                                        color: 'black',
                                        fontSize: 12
                                    }
                                },
                                color: options.color[0],
                                borderWidth: 8,
                                barBorderRadius: 8
                            }
                        },
                        markPoint: {
                            data: [{type: 'max', name: '最大值'}, {type: 'min', name: '最小值'}]
                        },
                        markLine: {
                            data: [{type: 'average', name: '平均值'}]
                        }
                    },
                    {
                        name: options.ldata[1],
                        type: 'line',
                        data: options.sdata2,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    position: 'top',
                                    textStyle: {
                                        color: 'black',
                                        fontSize: 12
                                    }
                                },
                                color: options.color[1],
                                borderWidth: 8,
                                barBorderRadius: 8
                            }
                        },
                        markPoint: {
                            data: [{type: 'max', name: '最大值'}, {type: 'min', name: '最小值'}]
                        },
                        markLine: {
                            data: [{type: 'average', name: '平均值'}]
                        }
                    }
                ]
            }
            comparativeDiscountMap.setOption(config);
            window.onresize = comparativeDiscountMap.resize;
        },
        pieChart: function (options) {
            var pieChartMap = echarts.init(document.getElementById(options.id), layui.echartsTheme);
            var config = {
                title: {
                    text: options.text,
                    x: 'center',
                    textStyle: {
                        fontSize: 14,
                        fontWeight: 'bolder',
                        color: '#333'
                    }
                },
                tooltip: {
                    trigger: 'item',
                    formatter: '{b}<br>金额：{c} ($) <br>占比：{d}%'
                },
                legend: {
                    orient: 'vertical',
                    x: 'left',
                    y: '30',
                    data: options.ldata
                },
                series: [{
                    name: '支出',
                    type: 'pie',
                    radius: '55%',
                    center: ['50%', '50%'],
                    data: options.sdata,
                    label: {
                        normal: {
                            show: true,
                            position: 'inner',
                            formatter: '{b}:{c}: ({d}%)'
                        }
                    },
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }]
            }
            pieChartMap.setOption(config);
            window.onresize = pieChartMap.resize;
        },
        panel: function (options) {
            if (options.data != null) {
                $(options.id).empty();
                var html = "";
                for (var i = 0; i < options.data.names.length; i++) {
                    html += "<li class=\"layui-col-xs3\"><a href=\"javascript:void(0);\" class=\"layadmin-backlog-body\"><h3>" + options.data.names[i] + "</h3><p><cite>" + options.data.moneys[i] + "</cite></p></a></li>";
                }
                $(options.id).append(html);
            }
        }
    }

    //监听路由
    layui.admin.on('hash(tab)', function (options) {
        layui.router().path.join('');
    });

    exports('console', home)
});