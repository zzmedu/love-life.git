package com.zou.after.dto;

import com.zou.after.dto.base.BaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class MoneyPayFindByPageDto extends BaseDto {

    private Long userId;
    private Long typeId;
    private Long modeId;
    private String store;
}
