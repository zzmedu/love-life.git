package com.zou.after.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zou.after.dto.MoneyIncomeFindByPageDto;
import com.zou.after.service.base.IFindByPageService;
import com.zou.after.vo.MoneyIncomeFindByPageVo;
import com.zou.after.entity.MoneyIncome;
/**
* com.zou.after服务类
* @author zouzhimin
* @date $date.get()
*/
public interface IMoneyIncomeService extends IService<MoneyIncome>, IFindByPageService<MoneyIncomeFindByPageDto, MoneyIncomeFindByPageVo>{
	
}