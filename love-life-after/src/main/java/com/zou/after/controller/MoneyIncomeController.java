package com.zou.after.controller;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.zou.after.dto.MoneyIncomeAddDto;
import com.zou.after.dto.MoneyIncomeUpdateDto;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.zou.after.dto.MoneyIncomeFindByPageDto;
import com.zou.after.entity.MoneyIncome;
import com.zou.after.service.IMoneyIncomeService;
import org.zzm.zlog.inter.ZLog;
import com.zou.security.base.http.Request;
import com.zou.security.base.http.Response;
import javax.validation.Valid;
import java.util.HashMap;

@RestController
@RequestMapping("/moneyIncome")
public class MoneyIncomeController {

    @Autowired
    private IMoneyIncomeService iMoneyIncomeService;
    
    @PostMapping("/findByPage")
    @ZLog(value = "分页查询")
    public Response findByPage(@Valid @RequestBody MoneyIncomeFindByPageDto moneyIncomeFindByPageDto){
        return Request.success(iMoneyIncomeService.findByPage(moneyIncomeFindByPageDto),iMoneyIncomeService.findByPageCount(moneyIncomeFindByPageDto));
    }

    @PostMapping("/add")
    @ZLog(value = "新增")
    public Response add(@Valid @RequestBody MoneyIncomeAddDto moneyIncomeAddDto){
        MoneyIncome moneyIncome = new MoneyIncome();
        BeanUtils.copyProperties(moneyIncomeAddDto, moneyIncome);
        moneyIncome.setUtime(DateUtil.now());
        if(iMoneyIncomeService.save(moneyIncome)){
            return  Request.success();
        }
        return Request.error();
    }

    @GetMapping("/isDelete/{id}/{status}")
    @ZLog(value = "逻辑删除数据")
    public Response isDelete(@PathVariable Integer id,@PathVariable Integer status){
        if(iMoneyIncomeService.update(new MoneyIncome(), new UpdateWrapper<MoneyIncome>().lambda()
                .set(MoneyIncome::getStatus,status)
                .eq(MoneyIncome::getId,id))){
            return  Request.success();
        }
        return Request.error();
    }
    
    @GetMapping("/delete/{id}")
    @ZLog(value = "非逻辑删除数据")
    public Response delete(@PathVariable Integer id){
        if(iMoneyIncomeService.remove(new QueryWrapper<MoneyIncome>().lambda().eq(MoneyIncome::getId,id))){
            return Request.success();
        }
        return Request.error();
    }

    @PostMapping("/update")
    @ZLog(value = "修改")
    public Response update(@Valid @RequestBody MoneyIncomeUpdateDto moneyIncomeUpdateDto){
        MoneyIncome moneyIncome = new MoneyIncome();
        BeanUtils.copyProperties(moneyIncomeUpdateDto,moneyIncome);
        moneyIncome.setUtime(DateUtil.now());
        if(iMoneyIncomeService.update(moneyIncome,new QueryWrapper<MoneyIncome>().lambda().eq(MoneyIncome::getId,moneyIncomeUpdateDto.getId()))){
            return Request.success();
        }
        return Request.error();
    }

    @GetMapping("/selectOne/{id}")
    @ZLog(value = "单个查询")
    public Response selectOne(@PathVariable Integer id){
        return Request.success(iMoneyIncomeService.getOne(new QueryWrapper<MoneyIncome>().lambda().eq(MoneyIncome::getId,id)));
    }

}