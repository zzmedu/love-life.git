package com.zou.security.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zou.security.base.pojo.SysUser;
import com.zou.security.base.vo.sysuser.FindByPageVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 邹智敏
 * @since 2020-08-11
 */
public interface ISysUserService extends IService<SysUser> {

    IPage<FindByPageVo> findByPage(Page<SysUser> page);

}
