package com.zou.security.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zou.security.base.dto.sysuser.SysUserDetailsDto;
import com.zou.security.base.pojo.SysUser;
import com.zou.security.base.vo.sysuser.FindByPageVo;
import com.zou.security.mapper.SysUserMapper;
import com.zou.security.service.ISysUserService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 邹智敏
 * @since 2020-08-11
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService, UserDetailsService {

    @Autowired
    private SysUserMapper sysUserMapper;

    @SneakyThrows
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SysUserDetailsDto sysUserDetailsDto = sysUserMapper.findUserByUsername(username);
        if (sysUserDetailsDto == null) {
            throw new UsernameNotFoundException("用户不存在");
        }
        return sysUserDetailsDto;
    }

    @Override
    public IPage<FindByPageVo> findByPage(Page<SysUser> page) {
        return sysUserMapper.findByPage(page);
    }
}
