package com.zou.after.vo;

import com.zou.after.entity.MoneyType;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 14:16 2019/7/19
 * </P>
 **/
@Data
@EqualsAndHashCode(callSuper = false)
public class MoneyTypeFindByPageVo extends MoneyType {
}
