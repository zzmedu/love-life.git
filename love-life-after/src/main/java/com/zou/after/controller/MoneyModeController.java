package com.zou.after.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.zou.after.dto.MoneyModeAddDto;
import com.zou.after.dto.MoneyModeFindByPageDto;
import com.zou.after.dto.MoneyModeUpdateDto;
import com.zou.after.entity.MoneyMode;
import com.zou.after.service.IMoneyModeService;
import com.zou.security.base.http.Request;
import com.zou.security.base.http.Response;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.zzm.zlog.inter.ZLog;

import javax.validation.Valid;
import java.util.HashMap;

@RestController
@RequestMapping("/moneyMode")
public class MoneyModeController {

    @Autowired
    private IMoneyModeService iMoneyModeService;
    
    @PostMapping("/findByPage")
    @ZLog(value = "分页查询")
    public Response findByPage(@Valid @RequestBody MoneyModeFindByPageDto moneyModeFindByPageDto){
        return Request.success(iMoneyModeService.findByPage(moneyModeFindByPageDto),iMoneyModeService.findByPageCount(moneyModeFindByPageDto));
    }

    @PostMapping("/add")
    @ZLog(value = "新增")
    public Response add(@Valid @RequestBody MoneyModeAddDto moneyModeAddDto){
        MoneyMode moneyMode = new MoneyMode();
        BeanUtils.copyProperties(moneyModeAddDto, moneyMode);
        if(iMoneyModeService.save(moneyMode)){
            return  Request.success();
        }
        return Request.error();
    }

    @GetMapping("/isDelete/{id}/{status}")
    @ZLog(value = "逻辑删除数据")
    public Response isDelete(@PathVariable Integer id,@PathVariable Integer status){
        if(iMoneyModeService.update(new MoneyMode(), new UpdateWrapper<MoneyMode>().lambda()
                .set(MoneyMode::getStatus,status)
                .eq(MoneyMode::getId,id))){
            return  Request.success();
        }
        return Request.error();
    }
    
    @GetMapping("/delete/{id}")
    @ZLog(value = "非逻辑删除数据")
    public Response delete(@PathVariable Integer id){
        if(iMoneyModeService.remove(new QueryWrapper<MoneyMode>().lambda().eq(MoneyMode::getId,id))){
            return Request.success();
        }
        return Request.error();
    }

    @PostMapping("/update")
    @ZLog(value = "修改")
    public Response update(@Valid @RequestBody MoneyModeUpdateDto moneyModeUpdateDto){
        MoneyMode moneyMode = new MoneyMode();
        BeanUtils.copyProperties(moneyModeUpdateDto,moneyMode);
        if(iMoneyModeService.update(moneyMode,new QueryWrapper<MoneyMode>().lambda().eq(MoneyMode::getId,moneyModeUpdateDto.getId()))){
            return Request.success();
        }
        return Request.error();
    }

    @GetMapping("/selectOne/{id}")
    @ZLog(value = "单个查询")
    public Response selectOne(@PathVariable Integer id){
        return Request.success(iMoneyModeService.getOne(new QueryWrapper<MoneyMode>().lambda().eq(MoneyMode::getId,id)));
    }

    @PostMapping("/selectList")
    @ZLog(value = "下拉查询")
    public Response selectList(){
        return  Request.success(iMoneyModeService.list(new QueryWrapper<MoneyMode>().lambda().select(MoneyMode::getId, MoneyMode::getName)));
    }
}