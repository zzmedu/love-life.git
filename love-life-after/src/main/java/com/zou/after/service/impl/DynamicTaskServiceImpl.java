package com.zou.after.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zou.after.dto.DynamicTaskFindByPageDto;
import com.zou.after.entity.DynamicTask;
import com.zou.after.mapper.DynamicTaskMapper;
import com.zou.after.service.IDynamicTaskService;
import com.zou.after.vo.DynamicTaskFindByPageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DynamicTaskServiceImpl extends ServiceImpl<DynamicTaskMapper, DynamicTask> implements IDynamicTaskService {

    @Autowired
    private DynamicTaskMapper dynamicTaskMapper;

    @Override
    public List<DynamicTaskFindByPageVo> findByPage(DynamicTaskFindByPageDto dynamicTaskFindByPageDto) {
        return dynamicTaskMapper.findByPage(dynamicTaskFindByPageDto);
    }

    @Override
    public Integer findByPageCount(DynamicTaskFindByPageDto dynamicTaskFindByPageDto) {
        return dynamicTaskMapper.findByPageCount(dynamicTaskFindByPageDto);
    }
}
