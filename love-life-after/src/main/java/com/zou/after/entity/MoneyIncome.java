package com.zou.after.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = false)
public class MoneyIncome {
	/** 编号 */
	@TableId(value = "id",type= IdType.AUTO)
	private Long id;
	/** 收入方式编号 */
	private Long modeId;
	/** 收入方式类型 */
	private Long typeId;
	/** 收入金额 */
	private BigDecimal money;
	/** 支付时间 */
	private String stime;
	/** 修改时间 */
	private String utime;
	/** 扣费商家 */
	private String store;
	/** 收入用户编号 */
	private Long userId;
	/** 逻辑删除；1：正常；2：删除*/
	private Integer status;
}