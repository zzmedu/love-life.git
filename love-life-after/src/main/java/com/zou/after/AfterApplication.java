package com.zou.after;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * @BelongsProject: love-life
 * @BelongPackage: PACKAGE_NAME
 * @Author: zouzhimin
 * @Date: 2020/11/25 10:58
 * @Description: TODO
 **/
@SpringBootApplication(scanBasePackages = "com.zou")
public class AfterApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(AfterApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(AfterApplication.class, args);
    }
}
