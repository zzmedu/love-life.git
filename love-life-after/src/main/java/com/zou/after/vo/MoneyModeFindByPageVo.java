package com.zou.after.vo;

import com.zou.after.entity.MoneyMode;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 14:16 2019/7/19
 * </P>
 **/
@Data
@EqualsAndHashCode(callSuper = false)
public class MoneyModeFindByPageVo extends MoneyMode {
}
