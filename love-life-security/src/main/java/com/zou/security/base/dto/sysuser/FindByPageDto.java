package com.zou.security.base.dto.sysuser;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 *
 * @Author：zouzhimin
 * @description：
 * @date：crealed in 9:35 2020/8/11
 * </P>
 **/
@Setter
@Getter
public class FindByPageDto extends com.zou.security.base.page.PageForm {
}
